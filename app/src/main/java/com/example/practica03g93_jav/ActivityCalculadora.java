package com.example.practica03g93_jav;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class ActivityCalculadora extends AppCompatActivity {

    private Button btnSumar;
    private Button btnRestar;
    private Button btnMultiplicar;
    private Button btnDividir;
    private Button btnLimpiar;
    private Button btnRegresar;
    private EditText txtNumero1;
    private EditText txtNumero2;
    private TextView lblResultado;
    private Calculadora calculadora = new Calculadora(0,0);

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora);

        iniciarComponentes();

        btnSumar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BtnSumar();
            }
        });

        btnRestar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BtnRestar();
            }
        });

        btnDividir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BtnDividir();
            }
        });

        btnMultiplicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BtnMultiplicar();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Limpiar();
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Regresar();
            }
        });

    }

    private void Limpiar(){
        txtNumero1.setText("");
        txtNumero2.setText("");
        lblResultado.setText("");
    }

    private void Regresar(){
        AlertDialog.Builder confirmar = new AlertDialog.Builder( this);
        confirmar.setTitle("Calculadora");
        confirmar.setMessage("¿Seguro de querer regresar?");

        confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent con2 = new Intent(ActivityCalculadora.this, MainActivity.class);
                startActivity(con2);
            }
        });
        confirmar.show();
    }

    private void iniciarComponentes(){
        btnSumar = findViewById(R.id.btnSumar);
        btnRestar = findViewById(R.id.btnRestar);
        btnMultiplicar = findViewById(R.id.btnMultiplicar);
        btnDividir = findViewById(R.id.btnDividir);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);
        txtNumero1 = findViewById(R.id.txtNumero1);
        txtNumero2 = findViewById(R.id.txtNumero2);
        lblResultado = findViewById(R.id.lblResultado);
    }

    private void BtnSumar(){
        String Valor1T = txtNumero1.getText().toString();
        String Valor2T = txtNumero2.getText().toString();

        if(!Valor1T.isEmpty() && !Valor2T.isEmpty()){
            calculadora.setNumero1(Float.parseFloat(Valor1T));
            calculadora.setNumero2(Float.parseFloat(Valor2T));
            lblResultado.setText(String.valueOf(calculadora.Suma()));
        }
        else{
            Toast.makeText( this, "capture ambos números", Toast.LENGTH_SHORT).show();
        }
    }

    private void BtnRestar(){
        String Valor1T = txtNumero1.getText().toString();
        String Valor2T = txtNumero2.getText().toString();

        if(!Valor1T.isEmpty() && !Valor2T.isEmpty()){
            calculadora.setNumero1(Float.parseFloat(Valor1T));
            calculadora.setNumero2(Float.parseFloat(Valor2T));
            lblResultado.setText(String.valueOf(calculadora.Resta()));
        }
        else{
            Toast.makeText( this, "capture ambos números", Toast.LENGTH_SHORT).show();
        }
    }

    private void BtnMultiplicar(){
        String Valor1T = txtNumero1.getText().toString();
        String Valor2T = txtNumero2.getText().toString();

        if(!Valor1T.isEmpty() && !Valor2T.isEmpty()){
            calculadora.setNumero1(Float.parseFloat(Valor1T));
            calculadora.setNumero2(Float.parseFloat(Valor2T));
            lblResultado.setText(String.valueOf(calculadora.Multiplicacion()));
        }
        else{
            Toast.makeText( this, "capture ambos números", Toast.LENGTH_SHORT).show();
        }
    }

    private void BtnDividir(){
        String Valor1T = txtNumero1.getText().toString();
        String Valor2T = txtNumero2.getText().toString();

        if(!Valor1T.isEmpty() && !Valor2T.isEmpty()){
            calculadora.setNumero1(Float.parseFloat(Valor1T));
            calculadora.setNumero2(Float.parseFloat(Valor2T));
            lblResultado.setText(String.valueOf(calculadora.division()));
        }
        else{
            Toast.makeText( this, "capture ambos números", Toast.LENGTH_SHORT).show();
        }
    }


}
