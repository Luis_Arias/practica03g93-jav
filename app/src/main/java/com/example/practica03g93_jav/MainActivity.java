package com.example.practica03g93_jav;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button btnCerrar;
    private EditText txtUsuario;
    private EditText txtContrasena;
    private Button btnIngresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        iniciarComponentes();

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cerrar();
            }
        });

        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ingresar();
            }
        });


    }
    private void iniciarComponentes(){
        btnIngresar = findViewById(R.id.btnIngresar);
        btnCerrar = findViewById(R.id.btnCerrar);
        txtContrasena = findViewById(R.id.txtContrasena);
        txtUsuario = findViewById(R.id.txtUsuario);
    }

    private void ingresar(){
        String strUsuario;
        String strContra;

        strUsuario= getResources().getString(R.string.usuario);
        strContra = getResources().getString(R.string.contrasena);

        if (txtUsuario.getText().toString().equals(strUsuario) && txtContrasena.getText().toString().equals(strContra)){
            Bundle con = new Bundle();
            con.putString("usuario", strUsuario);

            Intent con2 = new Intent(MainActivity.this, ActivityCalculadora.class);
            con2.putExtras(con);
            startActivity(con2);
        }
        else{
            Toast.makeText(getApplicationContext(), "Usuario o contraseña no válidos", Toast.LENGTH_LONG).show();
        }
    }
    private void cerrar(){
        finish();
    }


}