package com.example.practica03g93_jav;

public class Calculadora {

    private float Numero1;

    private float Numero2;

    public float getNumero1(){return Numero1;}

    public void setNumero1(float Numero1){this.Numero1 = Numero1;}

    public float getNumero2(){return Numero2;}

    public void setNumero2(float Numero2){this.Numero2 = Numero2;}

    public Calculadora(float Numero1, float Numero2){
        this.Numero2 = Numero2;
        this.Numero1 = Numero1;
    }

    public float Suma(){return Numero1 + Numero2;}
    public float Resta(){return Numero1 - Numero2;}
    public float Multiplicacion(){return Numero1 * Numero2;}
    public float division(){

        float total = 0;
        if (Numero2 !=0){
            total = Numero1 / Numero2;
        }
        return total;
    }

}
